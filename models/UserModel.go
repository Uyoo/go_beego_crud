package models

import (
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)

// User table 구성
type User struct {
	Id       int
	Username string
	Email    string
}

func init() {
	orm.RegisterModel(new(User))
}

// 모든 user의 정보 조회
func GetAllUsers() []*User {
	o := orm.NewOrm()
	var users []*User
	o.QueryTable(new(User)).All(&users)

	return users
}

// user의 정보 저장(create)
func InsertOneUser(user User) *User {
	o := orm.NewOrm()
	qs := o.QueryTable(new(User))

	// get prepared statement
	i, _ := qs.PrepareInsert()

	// Insert
	var u User
	id, err := i.Insert(&user)
	if err == nil {
		// successfully inserted
		u = User{Id: int(id)}
		err := o.Read(&u)
		if err == orm.ErrNoRows {
			return nil
		}
	} else {
		return nil
	}

	return &u
}

// 해당 id를 가진 user의 정보 수정
func UpdateUser(user User, id int) *User {
	o := orm.NewOrm()
	u := User{Id: id}
	var updatedUser User

	// get existing user
	if o.Read(&u) == nil {

		// updated user
		u = user
		u.Id = id
		_, err := o.Update(&u)

		// read updated user
		if err == nil {
			// update successful
			updatedUser = User{Id: u.Id}
			o.Read(&updatedUser)
		}
	}

	return &updatedUser
}

// 해당 id를 가진 user의 정보 삭제
func DeleteUser(id int) bool {
	o := orm.NewOrm()
	_, err := o.Delete(&User{Id: id})
	if err == nil {
		// successfull
		return true
	}

	return false
}

// 해당 id를 가진 user의 정보 조회
func GetUserById(id int) *User {
	o := orm.NewOrm()
	user := User{Id: id}
	o.Read(&user)
	return &user
}
