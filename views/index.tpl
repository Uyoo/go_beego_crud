<!DOCTYPE html>

<html>
<head>
  <title>Beego</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">  

  <style type="text/css">
    *,body {
      margin: 0px;
      padding: 0px;
    }

    body {
      margin: 0px;
      font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
      font-size: 14px;
      line-height: 20px;
      background-color: #fff;
    }        

    a {
      color: #444;
      text-decoration: none;
    }

    li {
      border: 1px solid grey; 
      width: fit-content;  
      margin: 10px;
      list-style: none;
    }

  </style>
</head>

<body>

  <ul>
    {{range .data}}
        <li>
          username: {{.Username}} <br />
          email: {{.Email}}
        </li>
    {{end}}
  </ul>

  <script src="/static/js/reload.min.js"></script>
</body>
</html>
