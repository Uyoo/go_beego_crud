package controllers

import (
	"beego_tutorial/models"
	"strconv"

	"github.com/astaxie/beego"
)

type UserController struct {
	beego.Controller
}

// 모든 user의 정보 조회
func (uc *UserController) GetAllUsers() {
	// === json mode ===
	// uc.Data["json"] = models.GetAllUsers()
	// uc.ServeJSON()

	// === view mode ===
	uc.Data["data"] = models.GetAllUsers()
	uc.TplName = "index.tpl"
}

// user의 정보 저장(create)
func (uc *UserController) AddNewUser() {

	var u models.User
	u.Username = uc.GetString("username")
	u.Email = uc.GetString("email")

	// json.Unmarshal(uc.Ctx.Input.RequestBody, &u)
	user := models.InsertOneUser(u)
	uc.Data["json"] = user
	uc.ServeJSON()
}

// 해당 id를 가진 user의 정보 수정
func (uc *UserController) UpdateUser() {

	// get id from query string and convert it to int
	id, _ := strconv.Atoi(uc.Ctx.Input.Param(":id"))

	var u models.User
	u.Username = uc.GetString("username")
	u.Email = uc.GetString("email")

	// json.Unmarshal(uc.Ctx.Input.RequestBody, &u)
	user := models.UpdateUser(u, id)
	uc.Data["json"] = user
	uc.ServeJSON()
}

// 해당 id를 가진 user의 정보 삭제
func (uc *UserController) DeleteUser() {

	// get id from query string and convert it to int
	id, _ := strconv.Atoi(uc.Ctx.Input.Param(":id"))

	// delete user
	deleted := models.DeleteUser(id)

	// generate response
	uc.Data["json"] = map[string]bool{"deleted": deleted}
	uc.ServeJSON()
}

// 해당 id를 가진 user의 정보 조회
func (uc *UserController) GetUserById() {
	// get the id from query string
	id, _ := strconv.Atoi(uc.Ctx.Input.Param(":id"))

	// get user
	user := models.GetUserById(id)

	// generate response
	uc.Data["json"] = user
	uc.ServeJSON()
}
