package routers

import (
	"beego_tutorial/controllers"

	"github.com/astaxie/beego"
)

func init() {
	// init namespace
	ns := beego.NewNamespace("/api",

		beego.NSNamespace("/users",
			// 모든 user의 정보 조회
			beego.NSRouter("/", &controllers.UserController{}, "get:GetAllUsers"),

			// user의 정보 저장(create)
			beego.NSRouter("/", &controllers.UserController{}, "post:AddNewUser"),

			// 해당 id를 가진 user의 정보 수정
			beego.NSRouter("/:id", &controllers.UserController{}, "put:UpdateUser"),

			// 해당 id를 가진 user의 정보 삭제
			beego.NSRouter("/:id", &controllers.UserController{}, "delete:DeleteUser"),

			// 해당 id를 가진 user의 정보 조회
			beego.NSRouter("/:id", &controllers.UserController{}, "get:GetUserById"),
		),
	)

	// register namespace
	beego.AddNamespace(ns)
}
